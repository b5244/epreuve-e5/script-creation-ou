﻿# Ce script permet la création des OU pour le domaine d'un ETP
# Son utilisation est "./creation_ou.ps1 nom1,nom2,nom3,... OU=dn,OU=del'emplacement,DC=chasseuneuil,DC=int"
# Il faut écrire le nom des OU que l'on veut créer dans séparés par des virgules sans espaces.
# Et le chemin ensuite (laisser vide si la création doit se faire à la racine)
# EXEMPLE : ".\creation_ou.ps1 Esporting,3DPrint86 OU=clients_entreprises,DC=chasseuneuil,DC=int"

param(
[array]$names,
[string]$path
)

#  Si je le chemin est vide on met le chemin de la racine
if ($path -eq "")
{
 $path = (Get-ADDomain -Current LocalComputer).DistinguishedName
}

# On crée la structure basique pour chaque nom donné
foreach ($name in $names)
{
    New-ADOrganizationalUnit -Name $name -Path $Path
    New-ADOrganizationalUnit -Name "utilisateurs" -Path "ou=$name,$path"
    New-ADOrganizationalUnit -Name "responsables" -Path "ou=utilisateurs,ou=$name,$path"
    New-ADOrganizationalUnit -Name "groupes globaux" -Path "ou=$name,$path"
    New-ADGroup "employes_$name" -Path "ou=groupes globaux,ou=$name,$path" -GroupScope Global
    New-ADGroup "responsables_$name" -Path "ou=groupes globaux,ou=$name,$path" -GroupScope Global
    New-ADOrganizationalUnit -Name "employes" -Path "ou=utilisateurs,ou=$name,$path"
    New-ADUser -Name "employe test" -GivenName "test" -Surname "employe" -UserPrincipalName "e_$name@chasseneuil.int" -SamAccountName "e_$name" -Path "ou=employes,ou=utilisateurs,ou=$name,$path" -AccountPassword(ConvertTo-SecureString "P@ssword1" -AsPlainText -Force) -Enabled $true -ChangePasswordAtLogon $true -PassThru | ForEach-Object {Add-ADGroupMember -Identity "cn=employes_$name,ou=groupes globaux,ou=$name,$path" -Members $_ -Verbose}
    New-ADUser -Name "responsable test" -GivenName "test" -Surname "responsable" -UserPrincipalName "r_$name@chasseneuil.int" -SamAccountName "r_$name" -Path "ou=responsables,ou=utilisateurs,ou=$name,$path" -AccountPassword(ConvertTo-SecureString "P@ssword1" -AsPlainText -Force) -Enabled $true -ChangePasswordAtLogon $true -PassThru | ForEach-Object {Add-ADGroupMember -Identity "cn=responsables_$name,ou=groupes globaux,ou=$name,$path" -Members $_ -Verbose}
    New-ADOrganizationalUnit -Name "ordinateurs" -Path "ou=$name,$path"
    New-ADObject -Type "computer" -Name "ordinateur_test" -Path "ou=ordinateurs,ou=$name,$path"
}
