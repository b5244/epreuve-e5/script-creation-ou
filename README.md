# Description

Ce script crée la structure des entités voulues dans l'arborescence de l'Active Directory. Sont utilisation est assez simple, il suffit de se placer dans un terminal dans le dossier contenant le script, puis de l'invoquer avec ses arguments

```
./creation_ou.ps1 <liste_des_entités> {<dn_de_l_entité_parente>}
```

La liste des entités doit être donnée avec des virgules comme séparateurs, et le chemin de l'entité parente est facultatif.

# Exemples

```
./creation_ou.ps1 administration
```

Créera l'arborescence du service Administration à la racine du domaine :

```
dc=chasseneuil,dc=tierslieux86,dc=fr
|-ou=administration
  |-ou=groupes globaux
  | |-cn=employes_administration
  |   |-cn=responsables_administration
  |-ou=ordinateurs
  | |-cn=ordinateur_test
  |-ou=utilisateurs
    |-ou=employes
    | |-cn=employe test
    |-ou=responsables
      |-cn=responsable test
```

```
./creation_ou.ps1 administration,adherents ou=ou_test,dc=chasseneuil,dc=tierslieux86,dc=fr
```

Créera les arborescences des services administration et adhérents dans l'ou "ou_test" :

```
dc=chasseneuil,dc=tierslieux86,dc=fr
|-ou=ou_test
  |-ou=administration
  | |-ou=groupes globaux
  | | |-cn=employes_administration
  | | |-cn=responsables_administration
  | |-ou=ordinateurs
  | | |-cn=ordinateur_test
  | |-ou=utilisateurs
  |   |-ou=employes
  |   | |-cn=employe test
  |   |-ou=responsables
  |     |-cn=responsable test
  |-ou=adherents
    |-ou=groupes globaux
    | |-cn=employes_administration
    | |-cn=responsables_administration
    |-ou=ordinateurs
    | |-cn=ordinateur_test
    |-ou=utilisateurs
      |-ou=employes
      | |-cn=employe test
      |-ou=responsables
        |-cn=responsable test  
```













